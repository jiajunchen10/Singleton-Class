this is a singleton class practice. There are 4 important characteristics in a singleton class, which are:
1. private static class instance
2. privte constructor
3. static method that provides access to the private static class instance
4. Synchronization