package singleton;

public class eagerInitialization extends Thread{
	private int count;       
	private static eagerInitialization instance = new eagerInitialization();
	private eagerInitialization(){
		System.out.println("a eagerInitialization object!");
	}
	public static eagerInitialization getInstance(){
		return instance;
	}
	public synchronized void increaseCount(){
		this.count +=1;
	}
	public synchronized void decreaseCount(){
		this.count-=1;
	}
	public synchronized int getCount(){
		return count;
	}
	public String toString(){
		return "eagerInitialization count is: "+count;
	}
	public static void main(String args[]){
		eagerInitialization mySingleton = eagerInitialization.getInstance();
		eagerInitialization mySingleton2 = eagerInitialization.getInstance();
		mySingleton.increaseCount();
		mySingleton2.increaseCount();
		System.out.println(mySingleton.toString());
		System.out.println(mySingleton2.toString());
	}
}
/*
 *   - the important things in a singleton class: 
 *   1. private static singleton instance
 *   2. private constructor
 *   3. static method getInstance()
 *   
 *   4. Synchronization!
 */
